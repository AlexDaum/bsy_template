.PHONY: debug release clean sanitized

exec=Test

sanitized:
	cd sanitized && exe_name=$(exec) make debug
	ln -sf sanitized/$(exec) ./$(exec)

debug:
	cd debug && make debug
	ln -sf debug/$(exec) ./$(exec)

release:
	cd release && make release
	ln -sf release/$(exec) ./$(exec)

clean:
	cd debug && make clean
	cd release && make clean
	cd sanitized && make clean
	rm ./$(exec)
